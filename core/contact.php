<?php
require(dirname(dirname(__FILE__))."/libs/phpmailer/PHPMailerAutoload.php");

if(isset($_POST['name'], $_POST['subject'], $_POST['email'], $_POST['message'])) {
    $name = $_POST['name'];
    $subject = $_POST['subject'];
    $email_reply = $_POST['email'];
    $message = $_POST['message'];

    if($name != "" && $subject != "" && $email_reply != "" && $message != "") {

        $subject = "Nuovo messaggio sul sito ZetaDue SNC!";
        $msg = "Nuovo messaggio sul sito ZetaDue SNC!
        <br /><br />
        Nome: ".$name."<br />
        Email: ".$email_reply."<br />
        Oggetto: ".$subject."<br />
        Messaggio: ".$message."<br />";



        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);// TCP port to connect to
        $mail->Sender = "info@zetaduesnc.it";

        $mail->ClearAllRecipients();
        $mail->From = "info@zetaduesnc.it";
        $mail->FromName = 'ZetaDue SNC';
        $mail->addAddress("info@zetaduesnc.it");
        $mail->AddReplyTo($email_reply, $name);
        $mail->Subject = $subject;
        $mail->Body = $msg;

        $second = $mail->send();

        if($second) {
            echo "Il tuo messaggio è stato inviato! Sarai ricontattato al più presto";
        } else {
            echo "Errore. Riprova più tardi";
        }
    } else {
        echo "Errore: tutti i campi sono obbligatori";
    }
} else {
    echo "Errore. Tutti i campi sono obbligatori";
}
?>
