<?php
$page = "home";

if(isset($_GET['page'])) {
    $page = $_GET['page'];
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Zeta Due SNC</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Zeta Due è un'azienda italiana specializzata nella costruzione e modifica di stampi tramite l’utilizzo di macchine manuali." />
        <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <!-- css -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/main.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Theme skin -->
        <link id="t-colors" href="skins/default.css" rel="stylesheet" />


        <script src="js/jquery.min.js"></script>
    </head>
    <body>
        <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v2.9&appId=1093809920643487";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <div id="wrapper">
            <!-- start header -->
            <header>
                <div class="top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="topleft-info">
                                    <li><i class="fa fa-map-marker"></i> Via P. Vigano’ 28/A, 31031 Caerano di S. Marco (TV) &nbsp; &nbsp; &nbsp; <i class="fa fa-phone"></i> 0423 859688 &nbsp; &nbsp; &nbsp; <a href="mailto:info@zetaduesnc.it"><i class="icon-envelope-alt"></i> info@zetaduesnc.it</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="home"><img src="img/logo.png" alt="" height="52" /></a>
                        </div>
                        <div class="navbar-collapse collapse ">
                            <ul class="nav navbar-nav">
                                <li <?php if($page=="home") echo 'class="active"'; ?>><a href="home">Home</a></li>
                                <li <?php if($page=="azienda") echo 'class="active"'; ?>><a href="azienda">Azienda</a></li>
                                <li <?php if($page=="servizi") echo 'class="active"'; ?>><a href="servizi">Servizi</a></li>
                                <li <?php if($page=="gallery") echo 'class="active"'; ?>><a href="gallery">Gallery</a></li>
                                <li <?php if($page=="contatti") echo 'class="active"'; ?>><a href="contatti">Contatti</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <!-- end header -->

            <?php
            include($page.".php");
            ?>


            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-lg-4">
                            <div class="widget">
                                <h4>Contattaci</h4>
                                <address>
                                    <strong>Zeta Due SNC</strong><br>
                                    <i class="fa fa-map-marker"></i> Via P. Vigano’ 28/A <br>
                                    31031 Caerano di S. Marco (TV)</address>
                                <p>
                                    <i class="fa fa-phone"></i> 0423 859688 <br>
                                    <i class="icon-envelope-alt"></i> info@zetaduesnc.it <br />
                                    <i class="icon-file-text"></i> P.IVA / C.F. 02086250269
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-4">
                            <div class="widget">
                                <h4>Pagine</h4>
                                <ul class="link-list">
                                    <li><a href="home">Home</a></li>
                                    <li><a href="azienda">Azienda</a></li>
                                    <li><a href="servizi">Servizi</a></li>
                                    <li><a href="gallery">Gallery</a></li>
                                    <li><a href="contatti">Contatti</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-4">
                            <div class="widget">
                                <h4>Seguici su Facebook</h4>
                                <div class="fb-page" data-href="https://www.facebook.com/zetaduesnc/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sub-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="copyright">
                                    <p>&copy; Zeta Due SNC - Tutti i diritti riservati</p>
                                    <div class="credits">
                                        Sito web realizzato da <a href="http://www.igsolutions.it">IG Solutions</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <ul class="social-network">
                                    <li><a href="https://www.facebook.com/Zeta-Due-SNC-222318168251721/" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

        <!-- Placed at the end of the document so the pages load faster -->

        <script src="js/modernizr.custom.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="plugins/flexslider/jquery.flexslider-min.js"></script>
        <script src="plugins/flexslider/flexslider.config.js"></script>
        <script src="js/jquery.appear.js"></script>
        <script src="js/stellar.js"></script>
        <script src="js/classie.js"></script>
        <script src="js/uisearch.js"></script>
        <script src="js/google-code-prettify/prettify.js"></script>
        <script src="js/animate.js"></script>
        <script src="js/custom.js"></script>


    </body>
</html>
