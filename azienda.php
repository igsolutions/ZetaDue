<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                    <li class="active">Azienda</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Storia</h3>
                <div style="float: left; margin-right: 15px;">
                    <img src="img/gallery/home.jpg" height="450" class="rounded-img" />
                </div>

                <p>Nel Maggio del 1988 I <b>fratelli Aldo e Ilario Zardin</b>, provenienti da una

                    <b>decennale esperienza nel settore metalmeccanico</b>, fondano la <b>ZETA DUE

                        SNC</b> sul ramo costruzione stampi per materie plastiche.<br /><br />

                        La nuova realtà si avvia semplicemente con poche macchine (pantografo,

                        saldatrice e banchi) e sull'operato dei fratelli Zardin; si presentava quindi

                        come un'azienda familiare basata sulla volontà di <b>progredire ed innovarsi</b>

                        con l'obiettivo di soddisfare le esigenze della clientela.<br /><br />

                        Con il passare degli anni i Sig.i Zardin riuscirono ad integrare nuove risorse

                        acquistando <b>molteplici apparecchiature e macchinari</b> sia in <b><a href="servizi&sezione=parco">reparto

                            produzione</a></b> che in <b><a href="servizi">ufficio tecnico</a></b>. <br />Di conseguenza s'integrarono anche nuove figure

                            nell'azienda in tutti i reparti arrivando al giorno d'oggi con stimata

                            collaborazione interna.<br /><br />

                            Dopo aver consolidato la propria <b>esperienza nella costruzione e modifica

                                stampi</b> del settore calzaturiero e in particolar modo su scarponi da sci, la

                                ZETA DUE si rivolge a un mercato più ampio arrivando al settore

                                <b><a href="servizi">automotive, elettrodomestici, medicale, articoli tecnici, moto</a></b>.<br /><br />

                                La <b>Zeta Due Snc</b> rivolge particolare attenzione all’innovazione tecnologica e

                                l’utilizzo di macchinari 3- 5 assi e di strumenti sempre più avanzati di

                                progettazione e modellazione tridimensionale. Ciò significa, in una situazione

                                concorrenziale di mercato, garantire in ogni momento un <b>prodotto di qualità</b>,

                                affidabile, al giusto prezzo, fornire un servizio ineccepibile, inoltre essendo in

                                grado di essere propositivi rispondere in modo tempestivo e con la <b>massima

                                    disponibilità alle richieste del Cliente</b>.
                                </p><br /><br /><br />
                            </div>
                            <div class="col-lg-6"><img class="rounded-img" src="img/chisiamo_2.jpg" alt="" width="100%" /></div>
                            <div class="col-lg-6"><br /><img class="rounded-img" src="img/chisiamo_3.jpg" alt="" width="100%" /></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <img src="img/progettazione.JPG" width="100%" class="rounded-img" />
                            </div>

                            <div class="col-sm-7">
                                <h3>Progettazione</h3>
                                <p>La Zeta Due Snc cura lo sviluppo del prodotto seguendo assieme al cliente l’evoluzione sin dalle prime fasi grazie all’ausilio dei propri sistemi
                                    <ul>
                                        <li>CAD-CAM</li>
                                        <li>POWER-SHAPE</li>
                                        <li>POWER-MILL</li>
                                        <li>SOLID WORKS</li>
                                    </ul>

                                    Dispone inoltre del sistema REVERSE-ENGINEERING per la ricostruzione di superfici matematiche da una nuvola di punti.</p><br />
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-sm-6">
                                    <h3>I macchinari</h3>
                                    <ul>
                                        <li>MORI SEIKI SV500</li>

                                        <li>MORI SEIKI NV5000 PALETIZZATO</li>

                                        <li>PAVENTA 1200 PLUS</li>

                                        <li>RETIFICA MINI DELTA</li>

                                        <li>EROSIONE AEG ELBOMAT</li>

                                        <li>TORNIO MANUALE</li>
                                    </ul>
                                </div>

                                <div class="col-sm-6">
                                    <img src="img/azienda_3.jpg" width="100%" class="rounded-img" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div align="center"><a href="servizi" class="btn btn-theme">Vedi i settori dove operiamo</a></div>
                                </div>
                            </div>
</div></div>
